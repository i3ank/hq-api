const authorize = (path, status) =>{
    return ((req, res, next) => {
        if(req.session.isLogined === status || (!req.session.isLogined && status === false)){
            next()         
        }else{
            res.cookie('flash_message', 'Please Login to access',{maxAge:3000})
            return res.redirect(path)      
        }
    })
}
module.exports = { authorize }