const express = require('express');
const app = express();
const path = require('path');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const store = '';
const { authorize } = require('./config/auth');
const createError = require('http-errors');

const port = 3000;
app.set('port', port);
  
const indexRouter = require('./routes/index');
const ordersRouter = require('./routes/order');
const registerRouter = require('./routes/register');
  
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('view options', {delimiter: '?'});
// app.set('env','production')

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

app.use(cookieParser());

//app.set('trust proxy', 1) // trust first proxy
app.use(session({
    name:'sid',
    secret: 'my ses secret',
    store:store,
    resave: true,
    saveUninitialized: true
}))
 
app.use('/', indexRouter);
app.use('/api/orders',  ordersRouter);
app.use('/api/register', registerRouter);

app.use(function(req, res, next) {
    var err = createError(404)
    next(err)
});
  
app.use(function (err, req, res, next) {
    res.locals.pageData = {
        title:'Error Page'
    }    
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}
  
    res.status(err.status || 500);
    res.render('pages/error');
});
  
app.listen(port, function() {
    console.log(`Example app listening on port ${port}!`)
});

module.exports = app;