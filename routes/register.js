const express = require('express');
const router = express.Router();
const { validation, schema } = require('../validator/users');
const multer = require('multer');
const upload = multer({ dest: 'uploads/'});
const fs = require('fs');
const md5 = require('md5');
const Pool = require('pg').Pool
const {  connection } = require('../config/db');

var type = upload.single('image');
router.route('/')
    .get((req, res) => {
    res.json({ message: "Welcome to bezkoder application." });
  }); 
router.route('/create')
    .post((req, res) => { 


        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({
            username: Joi.string().min(3).max(30).required(),
            firstname: Joi.string().min(6).max(30).required(),
            lastname: Joi.string().min(3).max(30).required(),
            address1: Joi.string().email().required(),
            address2: Joi.string().email().required(),
            telephone:Joi.string().min(6).max(10).required(),
            city: Joi.string().email().required(),
            state: Joi.string().email().required(),
            country: Joi.string().email().required(),
            email:Joi.string().allow('', null),
            postcode:Joi.string().min(1).max(5).required(),

        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                res.status(204).json('เกิดข้อผิดพลาด');
            } else {
                connection.query('SELECT username FROM distributor_login WHERE username = $1', [req.body.username], function (err, result) {
                    if (err) {
                        throw err
                      }
                      if (result.rows) { 
                        if(result.rows.length > 0) {
    
                            res.status(204).json('มี Username นี้ในระบบแล้ว');
    
                        } else {
                            
                            let password_hash = md5(req.body.password);

                            connection.query('INSERT INTO distributor_login (firstname, lastname, username, password, status, date_added) VALUES ($1, $2, $3, $4, $5, $6)', [req.body.firstname, req.body.lastname, req.body.username, password_hash, 0, new Date()], (error, results) => {
                                if (error) {
                                  throw error
                                }

                                connection.query('INSERT INTO distributor_file (username, file, date_added) VALUES ($1, $2, $3)', [req.body.username, target_path, new Date()], (error, results) => {
                                    if (error) {
                                      throw error
                                    }
                                });

                                connection.query('INSERT INTO distributor_telephone (username, telephone, date_added) VALUES ($1, $2, $3)', [req.body.username, req.body.telephone, new Date()], (error, results) => {
                                    if (error) {
                                      throw error
                                    }
                                });

                                connection.query('INSERT INTO distributor_email (username, email, date_added) VALUES ($1, $2, $3)', [req.body.username, req.body.email, new Date()], (error, results) => {
                                    if (error) {
                                      throw error
                                    }
                                });

                                connection.query('INSERT INTO distributor_line (username, line_id, date_added) VALUES ($1, $2, $3)', [req.body.username, req.body.line_id, new Date()], (error, results) => {
                                    if (error) {
                                      throw error
                                    }
                                });

                                connection.query('INSERT INTO distributor_parent (username, username_root, date_added) VALUES ($1, $2, $3)', [req.body.username, req.session.nameuser, new Date()], (error, results) => {
                                    if (error) {
                                      throw error
                                    }
                                });

                                res.status(200).json('ลงทะเบียนเรียบร้อยแล้ว');
                            });
                        }
                        
                    } else {
    
                        res.status(204).json('เกิดข้อผิดพลาด');
    
                    }
                  });
            }
        });
    })


module.exports = router