const express = require('express');
const router = express.Router();
const { validation, schema } = require('../validator/users');
const multer = require('multer');
const upload = multer({ dest: 'uploads/'});
const fs = require('fs');
const md5 = require('md5');
const Pool = require('pg').Pool
const {  connection } = require('../config/db');


router.route('/')
    .get((req, res, next) => {
        //req.renderPage = "pages/403"
        //next()
        return res.status(202).json({ message: "เกิดข้อผิดพลาด" });
  }); 
   
router.route('/create')
    .post((req, res, next) => {

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({

            distributor: Joi.string().required(),
            order_invoice: Joi.string().required(),
            total_price: Joi.string().required(),
            order_status: Joi.string().required(),
            order_date: Joi.string().required(),
            warehouse_id: Joi.string().required(),
            shipped_date:Joi.string().allow('', null),
            shipping_cost:Joi.string().allow('', null),
            courier_code:Joi.string().allow('', null),
            tracking_number:Joi.string().allow('', null),
            qty_product: Joi.string().required(),
            product_sku: Joi.string().required(),
            price_product: Joi.string().required(),

        });

        Joi.validate(data, schema, (err, value) => {

            if (err) {
                return res.status(202).json({ message: "เกิดข้อผิดพลาดในระบบ" });
            } else {

                let qty_product_array = req.body.qty_product;
                let product_sku_array = req.body.product_sku;
                let price_product_array = req.body.price_product;
    
    
               connection.query('INSERT INTO orders (distributor, order_invoice, total_price, order_status, order_date, shipped_date, warehouse_id, shipping_cost, courier_code, tracking_number, createdat) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING *', [req.body.distributor, req.body.order_invoice, req.body.total_price, req.body.order_status, req.body.order_date, req.body.shipped_date, req.body.warehouse_id, req.body.shipping_cost, req.body.courier_code, req.body.tracking_number, new Date()] , (error, results) => {
                    
                    if (error) {
                      throw error
                    }else{
                        
                         /* console.log('insert order_items',results) */

                         for(var i = 0; i < product_sku_array.length;i++){

                            connection.query('INSERT INTO order_items (order_id, product_sku, quantity, total_price ,createdat) VALUES ($1, $2, $3, $4, $5)', [results.rows[0].order_id, product_sku_array[i], qty_product_array[i], price_product_array[i], new Date()], (error, results) => {

                                console.log('insert order_items',error)

                                try {
                                    res.status(200).json({ message: "success" });
                                    //res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                                    //res.redirect('/admin/me');
                                
                                   
                                  } catch (error) {
                                    return res.status(202).json({ message: "เกิดข้อผิดพลาด" });
                                    
                                  }

                             });
                             
                           
                         }

                    }
                    
                });
      
            }
        });
    })

module.exports = router